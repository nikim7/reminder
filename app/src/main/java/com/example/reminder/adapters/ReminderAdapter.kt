package com.example.reminder.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.reminder.R
import com.example.reminder.data.entity.Reminder
import kotlinx.android.synthetic.main.one_time_reminder.view.short_description
import kotlinx.android.synthetic.main.reoccurring_reminder.view.*

import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat


class ReminderAdapter(var reminderList: List<Reminder>) :
    RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        context = parent.context
        val viewOne: View
        val viewRep: View
        if (viewType == 0) {
            viewOne = inflater.inflate(R.layout.reoccurring_reminder, parent, false)
            return ReminderViewHolder(viewOne)
        } else {
            viewRep = inflater.inflate(R.layout.reoccurring_reminder, parent, false)
            return ReminderViewHolder(viewRep)
        }
    }

    override fun getItemCount() = reminderList.size

    override fun onBindViewHolder(holder: ReminderViewHolder, position: Int) {

        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(context, reminderList[position].reminderIconColor),
                ContextCompat.getColor(context, R.color.colorPrimary)
            )
        )

        holder.view.ribbon_repeting_iv.setBackground(gradientDrawable)
        holder.view.ribbon_repeting.setImageResource(reminderList[position].reminderIcon)
        holder.view.tag_text.visibility = View.INVISIBLE
        holder.view.tag_text_center.visibility = View.INVISIBLE
        holder.view.tag_text.text = reminderList[position].reminderTag
        holder.view.reminder_countdown.visibility = View.GONE
        holder.view.exchange_icon.setImageResource(reminderList[position].reminderTypeIcon)
        holder.view.short_description.text = reminderList[position].reminderTitle
        holder.view.date_text.text = reminderList[position].reminderDate
        holder.view.time_text.text = reminderList[position].reminderTime

    }

    override fun getItemViewType(position: Int): Int {
        return if (reminderList[position].reminderType.equals("repeat")) {
            0
        } else {
            1
        }
    }

    class ReminderViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    fun updateAdapter(reminderList: List<Reminder>) {
        this.reminderList = reminderList
        notifyDataSetChanged()
    }

}