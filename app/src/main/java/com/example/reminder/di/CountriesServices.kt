package com.example.reminder.di

import com.example.reminder.model.CountriesApi
import com.example.reminder.model.Country
import io.reactivex.Single
import javax.inject.Inject

class CountriesServices {

    @Inject
     lateinit var api: CountriesApi

    init {
        DaggerApiComponent.create().inject(this)
    }

    fun getCountries(): Single<List<Country>>{
        return api.getUsers()
    }
}