package com.example.reminder.view.fragments.adderfragments

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.NumberPicker
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reminder.R
import com.example.reminder.adapters.SoundsAdapter
import com.example.reminder.databinding.FragmentReminderAdderBinding
import com.example.reminder.databinding.FragmentStandardAdderBinding
import com.example.reminder.model.SoundModel
import com.example.reminder.utilities.Constants
import com.example.reminder.viewmodel.AdderViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.squareup.timessquare.CalendarPickerView
import kotlinx.android.synthetic.main.alert_number_picker.view.*
import kotlinx.android.synthetic.main.alert_sound_picker.*
import kotlinx.android.synthetic.main.alert_sound_picker.view.*
import kotlinx.android.synthetic.main.date_picker_dialog.view.*
import kotlinx.android.synthetic.main.fragment_standard_adder.*
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class StandardAdderFragment : Fragment(), SoundsAdapter.OnSoundListener {

    lateinit var viewModel : AdderViewModel
    lateinit var selectedSoundModel : SoundModel
    lateinit var bottomSheetBehavior : BottomSheetBehavior<View>
    lateinit var soundsAdapter : SoundsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//         : View = inflater.inflate(R.layout.fragment_standard_adder, container, false)
        viewModel = ViewModelProviders.of(activity!!).get(AdderViewModel::class.java)
        val binding = FragmentStandardAdderBinding.inflate(inflater, container, false)
        binding.vm = viewModel
        val view = binding.root
        soundsAdapter = SoundsAdapter(arrayListOf(), this)
        selectedSoundModel = viewModel.ringToneList().get(0)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareStuff()
        observeTransparencyChanges()
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
//                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    viewModel.setTransparentBg(false)
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })

//        add_reminder_button.setOnClickListener{
//            viewModel.addReminder(short_note_et.text.toString(),start_date_tv.text.toString(),start_time_tv.text.toString())
//        }

        transparent_bg.setOnClickListener{
//            transparent_bg.visibility = View.GONE
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            viewModel.setTransparentBg(false)
        }

        sound_bt.setOnClickListener {
            sound_iv.setImageResource(R.drawable.alarm_on_icon)
            vibration_iv.setImageResource(R.drawable.vibration_off_icon)
            both_iv.setImageResource(R.drawable.sound_vibration_off_icon)
        }

        vibration_bt.setOnClickListener {
            sound_iv.setImageResource(R.drawable.alarm_off_icon)
            vibration_iv.setImageResource(R.drawable.vibration_on_icon)
            both_iv.setImageResource(R.drawable.sound_vibration_off_icon)
        }

        both_bt.setOnClickListener {
            sound_iv.setImageResource(R.drawable.alarm_off_icon)
            vibration_iv.setImageResource(R.drawable.vibration_off_icon)
            both_iv.setImageResource(R.drawable.sound_vibration_icon)
        }
    }

    fun observeTransparencyChanges() {
        viewModel.isTransparent.observe(this, androidx.lifecycle.Observer{ isTransparent ->
            isTransparent.let{
                if (isTransparent) {
                    transparent_bg.visibility = View.VISIBLE
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
                else {
                    transparent_bg.visibility = View.GONE
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                }
            }
        })
    }

    fun prepareStuff(){
        viewModel.refreshAdder()
        add_reminder_button.background = gradient(R.color.darkGray,context!!)

//        binder.reminderToneLayout.setOnClickListener{
//            Toast.makeText(context,"Push 1", Toast.LENGTH_SHORT).show()
//            binder.reminderToneTv.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.reminderToneTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.reminderToneLayout.windowToken, 0)
//        }

        start_date_tv.setOnClickListener{
            start_date_tv.requestFocus()
            val inputManager = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(start_date_tv.windowToken, 0)
            showDatePicker()
        }

        start_time_tv.setOnClickListener{
            start_time_tv.requestFocus()
            val inputManager = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(start_time_tv.windowToken, 0)
            showNumberPicker()
        }

        reminder_tone_tv.setOnClickListener{
            reminder_tone_tv.requestFocus()
            reminder_tone_tv.showSoftInputOnFocus = false
            val inputManager = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            activity!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
            inputManager.hideSoftInputFromWindow(reminder_tone_tv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.reminderToneLayout.windowToken, 0)
            showSoundPicker()
        }
    }

    fun showDatePicker(){
        val dialogView = LayoutInflater.from(context).inflate(R.layout.date_picker_dialog, null)
        val alertDialogBuilder = this.let { AlertDialog.Builder(context!!).setView(dialogView) }
        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
        val today = Date()
        val nextYear : Calendar = Calendar.getInstance()
        nextYear.add(Calendar.YEAR, 1)

        dialogView.calendar.init(today, nextYear.time)
            .inMode(CalendarPickerView.SelectionMode.SINGLE)
            .withSelectedDate(today)

        dialogView.calendar.setOnDateSelectedListener(object : CalendarPickerView.OnDateSelectedListener{
            override fun onDateSelected(date: Date?) {
                viewModel.setDate(date!!)
                alertDialog.cancel()
            }

            override fun onDateUnselected(date: Date?) {
            }
        })

        dialogView.done_layout.setOnClickListener{
            alertDialog.cancel()
        }
    }

    fun showNumberPicker(){
        val dialogView = LayoutInflater.from(context).inflate(R.layout.alert_number_picker, null)
        val alertDialogBuilder = this.let { AlertDialog.Builder(context!!).setView(dialogView) }
        val alertDialog : AlertDialog = alertDialogBuilder!!.show()

        dialogView.hour_picker.minValue = 0
        dialogView.hour_picker.maxValue = 23
        dialogView.hour_picker.displayedValues = Constants.hours
        dialogView.hour_picker.setOnValueChangedListener({ numberPicker: NumberPicker, i: Int, i1: Int ->
            dialogView.hours_view.text = numberPicker.value.toString()
        })

        dialogView.minutes_picker.minValue = 0
        dialogView.minutes_picker.maxValue = 59
        dialogView.minutes_picker.displayedValues = Constants.minutes
        dialogView.minutes_picker.setOnValueChangedListener({ numberPicker: NumberPicker, i: Int, i1: Int ->
            dialogView.minutes_view.text = numberPicker.value.toString()
        })

        dialogView.select.setOnClickListener {
            //            binder.startTimeTv.setText(dialogView.hours_view.text.toString() + ":" + dialogView.minutes_view.text.toString())
            viewModel.setTime(dialogView.hours_view.text.toString(), dialogView.minutes_view.text.toString())
            alertDialog.dismiss()
        }
    }

    private fun showSoundPicker() {

//        val dialogView = LayoutInflater.from(context).inflate(R.layout.alert_sound_picker, null)
//        val alertDialogBuilder = this.let { AlertDialog.Builder(context!!).setView(dialogView) }
//        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
//        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//        transparent_bg.visibility = View.VISIBLE
        viewModel.setTransparentBg(true)
        soundsAdapter.updateAdapter(viewModel.ringToneList())
        bottom_sheet.recycler_sound.setHasFixedSize(true)
        bottom_sheet.recycler_sound.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = soundsAdapter
        }
//        alertDialogBuilder.setOnDismissListener {
//            viewModel.releaseMediaPlayer()
//        }
//        alertDialog.setOnDismissListener {
//            viewModel.releaseMediaPlayer()
//        }
        select_sound_bt.setOnClickListener {
            viewModel.setTone(selectedSoundModel.soundTitle)
//            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
//            transparent_bg.visibility = View.GONE
            viewModel.setTransparentBg(false)
            viewModel.releaseMediaPlayer()
        }
    }

    fun gradient (color : Int, context : Context) : GradientDrawable {
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(context, color),
                ContextCompat.getColor(context, color))
        )
        gradientDrawable.cornerRadius = 10F
        return gradientDrawable
    }

    override fun onSongPlay(model: SoundModel) {
        this.selectedSoundModel = model
        viewModel.playTone(model.soundPath)
        soundsAdapter.notifyDataSetChanged()
    }

}
