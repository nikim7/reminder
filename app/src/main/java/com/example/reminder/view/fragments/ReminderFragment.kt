package com.example.reminder.view.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.NumberPicker
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reminder.R
import com.example.reminder.adapters.AdderAdapter
import com.example.reminder.adapters.ReminderAdapter
import com.example.reminder.adapters.SoundsAdapter
//import com.example.reminder.databinding.ReminderAdderBinding
import com.example.reminder.model.Adder
import com.example.reminder.model.SoundModel
import com.example.reminder.utilities.Constants
import com.example.reminder.utilities.FloatingHelper
import com.example.reminder.view.MainActivity
import com.example.reminder.view.ReminderAdderActivity
import com.example.reminder.viewmodel.ReminderViewModel
import com.google.android.material.textfield.TextInputLayout
import com.squareup.timessquare.CalendarPickerView
import kotlinx.android.synthetic.main.alert_number_picker.view.*
import kotlinx.android.synthetic.main.alert_sound_picker.view.*
import kotlinx.android.synthetic.main.date_picker_dialog.view.*
import kotlinx.android.synthetic.main.fragment_reminder.*
import java.util.*


class ReminderFragment : Fragment(), FloatingHelper.FloatingActionListener{

    private lateinit var viewModel: ReminderViewModel
    private val reminderAdapter = ReminderAdapter(arrayListOf())
//    private val adderAdapter = AdderAdapter(arrayListOf(), this)
    private lateinit var floatingButtonHelper: FloatingHelper
//    lateinit var binder: ReminderAdderBinding
//    lateinit var adder : Adder
//    lateinit var selectedSoundModel : SoundModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_reminder, container, false)
        viewModel = ViewModelProviders.of(this).get(ReminderViewModel::class.java)
//        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_reminder, container, false)
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        floatingButtonHelper = FloatingHelper(
            fab,
            fab1,
            fab2,
            repeating_tv,
            onetime_tv,
            floating_background
        )

        viewModel.refresh()

        context?.let { floatingButtonHelper.setListener(it, this) }
        observeViewModelList()

        reminder_recyclerview.apply {
            layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
            adapter = reminderAdapter
        }
    }

    fun observeViewModelList() {
        viewModel.reminderList.observe(this, Observer { reminderList ->
            reminderList?.let {
                reminder_recyclerview.visibility = View.VISIBLE
                reminderAdapter.updateAdapter(reminderList)
            }
        })
    }

//    fun observeAdderViewModelList() {
//        viewModel.adderList.observe(this, Observer { adderList ->
//            adderList?.let {
//                adderAdapter.updateAdapter(adderList)
//            }
//        })
//    }

//    fun observeDialogBoolean(){
//        viewModel.dialogShow.observe(this, Observer { dialogShow ->
//            dialogShow?.let {
//              if (!dialogShow){
////                  alertDialog.dismiss()
////                  included_reminder_adder.visibility = View.GONE
//                  activity!!.setContentView(R.layout.activity_main)
//                  showHideBackground(false)
//                  floatingButtonHelper.closeFloating()
//                  reminder_recyclerview.smoothScrollToPosition(0)
//              }else{}
//            }
//        })
//    }

//    fun observeDataChanges(){
//        viewModel.reminderDate.observe(this, Observer { reminderDate ->
//            reminderDate?.let {
//                viewModel.setDate()
//            }
//        })
//    }

    override fun oneTime() {

    }

    override fun repeating() {
//        included_reminder_adder.visibility = View.VISIBLE
        val i = Intent(activity, ReminderAdderActivity::class.java)
        startActivity(i)
//        activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
//        showDialog()
    }

    override fun showHideBackground(variable: Boolean) {
        (activity as MainActivity?)?.floatingBackground(variable)
    }

//    private fun showDialog() {
//        binder = DataBindingUtil.setContentView(activity!!,R.layout.reminder_adder)
//        binder.vm = viewModel
//        val alertDialogBuilder = context?.let { AlertDialog.Builder(it).setView(binder.root) }
//        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
//        binder.vm = viewModel
//        viewModel.refreshAdder()
//        binder.addReminderButton.background = gradient(R.color.darkGray,context!!)
//
//        binder.recyclerType.apply {
//            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//            adapter = adderAdapter }
//
//        binder.startDateLayout.setOnClickListener{
//            Toast.makeText(context,"Push 1", Toast.LENGTH_SHORT).show()
//            binder.startDateLayout.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.startDateTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.startDateLayout.windowToken, 0)
//            showDatePicker()
//        }
//
//        binder.startTimeLayout.setOnClickListener{
//            Toast.makeText(context,"Push 1", Toast.LENGTH_SHORT).show()
//            binder.startTimeTv.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.startTimeTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.startTimeLayout.windowToken, 0)
//        }

//        binder.reminderToneLayout.setOnClickListener{
//            Toast.makeText(context,"Push 1", Toast.LENGTH_SHORT).show()
//            binder.reminderToneTv.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.reminderToneTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.reminderToneLayout.windowToken, 0)
//        }

//        binder.startDateTv.setOnClickListener{
//            binder.startDateTv.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.startDateTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.startDateLayout.windowToken, 0)
//            showDatePicker()
//        }
//
//        binder.startTimeTv.setOnClickListener{
//            binder.startTimeTv.requestFocus()
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            inputManager.hideSoftInputFromWindow(binder.startTimeTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.startTimeLayout.windowToken, 0)
//            showNumberPicker()
//        }

//        binder.reminderToneTv.setOnClickListener{
//            binder.reminderToneTv.requestFocus()
//            binder.reminderToneTv.showSoftInputOnFocus = false
//            val inputManager = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            activity!!.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
//            inputManager.hideSoftInputFromWindow(binder.reminderToneTv.windowToken, 0)
//            inputManager.hideSoftInputFromWindow(binder.reminderToneLayout.windowToken, 0)
//            showSoundPicker()
//        }

//        observeAdderViewModelList()
//        observeDialogBoolean()
//    }

//    private fun showSoundPicker() {
//        val dialogView = LayoutInflater.from(context).inflate(R.layout.alert_sound_picker, null)
//        val alertDialogBuilder = context?.let { AlertDialog.Builder(it).setView(dialogView) }
//        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
//        val soundsAdapter = SoundsAdapter(arrayListOf(), this)
//        soundsAdapter.updateAdapter(viewModel.ringToneList())
//        dialogView.recycler_sound.apply {
//            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
//            adapter = soundsAdapter
//        }
//        alertDialogBuilder.setOnDismissListener {
//            viewModel.releaseMediaPlayer()
//        }
//        alertDialog.setOnDismissListener {
//            viewModel.releaseMediaPlayer()
//        }
//        dialogView.select_sound_bt.setOnClickListener {
//            viewModel.setTone(selectedSoundModel.soundTitle)
//            alertDialog.dismiss()
//        }
//    }

//    fun showDatePicker(){
//        val dialogView = LayoutInflater.from(context).inflate(R.layout.date_picker_dialog, null)
//        val alertDialogBuilder = context?.let { AlertDialog.Builder(it).setView(dialogView) }
//        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
//        val today = Date()
//        val nextYear : Calendar = Calendar.getInstance()
//        nextYear.add(Calendar.YEAR, 1)
//
//        dialogView.calendar.init(today, nextYear.time)
//            .inMode(CalendarPickerView.SelectionMode.SINGLE)
//            .withSelectedDate(today)
//
//        dialogView.calendar.setOnDateSelectedListener(object : CalendarPickerView.OnDateSelectedListener{
//            override fun onDateSelected(date: Date?) {
//                viewModel.setDate(date!!)
//                alertDialog.cancel()
//            }
//
//            override fun onDateUnselected(date: Date?) {
//            }
//        })
//
//        dialogView.done_layout.setOnClickListener{
//            alertDialog.cancel()
//        }
//    }

//    fun showNumberPicker(){
//        val dialogView = LayoutInflater.from(context).inflate(R.layout.alert_number_picker, null)
//        val alertDialogBuilder = context?.let { AlertDialog.Builder(it).setView(dialogView) }
//        val alertDialog : AlertDialog = alertDialogBuilder!!.show()
//
//        dialogView.hour_picker.minValue = 0
//        dialogView.hour_picker.maxValue = 23
//        dialogView.hour_picker.displayedValues = Constants.hours
//        dialogView.hour_picker.setOnValueChangedListener({ numberPicker: NumberPicker, i: Int, i1: Int ->
//            dialogView.hours_view.text = numberPicker.value.toString()
//        })
//
//        dialogView.minutes_picker.minValue = 0
//        dialogView.minutes_picker.maxValue = 59
//        dialogView.minutes_picker.displayedValues = Constants.minutes
//        dialogView.minutes_picker.setOnValueChangedListener({ numberPicker: NumberPicker, i: Int, i1: Int ->
//            dialogView.minutes_view.text = numberPicker.value.toString()
//        })
//
//        dialogView.select.setOnClickListener {
////            binder.startTimeTv.setText(dialogView.hours_view.text.toString() + ":" + dialogView.minutes_view.text.toString())
//            viewModel.setTime(dialogView.hours_view.text.toString(), dialogView.minutes_view.text.toString())
//            alertDialog.dismiss()
//        }
//    }

//    override fun onTypeSelector(adder: Adder, position: Int) {
//        this.adder = adder
//        viewModel.reminderType.set(position.toString())
//        if (binder.recyclerType.isShown){
//            setTextInputLayoutHintColor(binder.textInputLayout, context, adder.color)
//            setTextInputLayoutHintColor(binder.startDateLayout, context, adder.color)
//            setTextInputLayoutHintColor(binder.startTimeLayout, context, adder.color)
//            setTextInputLayoutHintColor(binder.typeLayout, context, adder.color)
////            setTextInputLayoutHintColor(binder.reminderToneLayout, context, adder.color)
//            binder.addReminderButton.background = gradient(adder.color,context!!)
//        }
//    }

    fun gradient (color : Int, context : Context) : GradientDrawable{
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.LEFT_RIGHT,
            intArrayOf(
                ContextCompat.getColor(context, color),
                ContextCompat.getColor(context, color))
        )
        gradientDrawable.cornerRadius = 10F
        return gradientDrawable
    }

    private fun setTextInputLayoutHintColor(textInputLayout: TextInputLayout, context: Context?, @ColorRes colorIdRes: Int) {
        textInputLayout.defaultHintTextColor = ColorStateList.valueOf(ContextCompat.getColor(context!!, colorIdRes))
    }

//    override fun onSongPlay(model: SoundModel) {
//        this.selectedSoundModel = model
//        viewModel.playTone(model.soundPath)
//    }


}
