package com.example.reminder.utilities

import android.os.Build
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateHelper {

    companion object {

        fun convertToDateaApi27orMore(date: String?, time: String): Date? {
            try {
                val format = SimpleDateFormat("MMM d, yyyy HH:mm")
                val newDate = format.parse(date + " " + time)
                return newDate
            } catch (e: ParseException) {
                e.printStackTrace()
                return null
            }
        }

        fun convertToDateaApi27orLess(date: String?, time: String): Date? {
            try {
                val format = SimpleDateFormat("d MMM yyyy HH:mm")
                val newDate = format.parse(date + " " + time)
                return newDate
            } catch (e: ParseException) {
                e.printStackTrace()
                return null
            }
        }

        fun dateDifference(date: String?, time: String): Long {
            val timeDiff : Long
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                timeDiff = convertToDateaApi27orLess(date, time)!!.time - System.currentTimeMillis()
            }else{
                timeDiff = convertToDateaApi27orMore(date, time)!!.time - System.currentTimeMillis()
            }
            return TimeUnit.MILLISECONDS.toSeconds(timeDiff)
        }

        fun getCalendarDate(date: String?, time: String) : Calendar{
            val calendar = Calendar.getInstance()
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1) {
                calendar.time = convertToDateaApi27orLess(date, time)
            }else{
                calendar.time = convertToDateaApi27orMore(date, time)
            }
            return calendar;
        }
    }
}